#include <algorithm>
#include <fstream>
#include "ConfigInterface.h"

ConfigInterface::ConfigInterface(std::string filePath)
{
    kCfg = GetConfigJson(filePath);
}

std::string ConfigInterface::GetName() { return kCfg["name"].asString(); }
Json::Value ConfigInterface::GetPackages() { return kCfg["dependencies"]; }
std::string ConfigInterface::GetString() { return kCfg.asString(); }

/* Private */
Json::Value
ConfigInterface::GetConfigJson(std::string path)
{
    std::ifstream f(path, std::ifstream::binary);
    Json::Value cfg;
    f >> cfg;
    return cfg;
}

