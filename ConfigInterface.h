#ifndef CONFIG_INTERFACE_H
#define CONFIG_INTERFACE_H

#include <json/json.h>
#include <string>

class ConfigInterface {
public:
    ConfigInterface(std::string filePath);

    // Get/set methods
    std::string GetName();
    Json::Value GetPackages();
    std::string GetString();

private:
    Json::Value GetConfigJson(std::string filePath);
    Json::Value kCfg;
};

#endif // CONFIG_INTERFACE_H

