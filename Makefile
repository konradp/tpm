CC = g++
OBJS = tpm.cc \
       ConfigInterface.cc \
       Support.cc \
       lib/jsoncpp.cpp

OBJ_NAME = tpm
FLAGS = -w -Wno-psabi -I ./lib -std=c++11

all: $(OBJS)
	g++ $(OBJS) $(FLAGS) -o $(OBJ_NAME)

debug: $(OBJS)
	g++ $(OBJS) $(FLAGS) -g -o $(OBJ_NAME)

install:
	cp tpm /usr/bin/

clean:
	rm tpm

