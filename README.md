# tpm: Tiny package manager

It allows you to add to your GitLab project's repository a single `package.json` file which specifies a list of dependencies for your project (other GitLab repositories/libraries), along with their required versions. After checking out the repository, running `tpm install` will create a `lib` dir containing these dependencies.

Note: Inspired by `npm` and Python's `pip install -r requirements.txt`.

## Installation
Clone this repository
```
    git clone https://gitlab.com/konradp/tpm.git
    cd tpm
```
then compile and install `tpm`
```
    make
    make install
```

## Usage: `package.json` format
inside your GitLab repository, create a `package.json` file adhering to the below format
```
{
    "name": "your-project-name",
    "version": "1.0.0",
    "homepage": "https://konradp.gitlab.io",
    "dependencies": {
       "package name1": "version-or-wildcard",
       "package name2": "*"
    }
}
```
where the 'package name' is a path to a dependency package on GitLab, see the exaple below (and also the `./examples` dir in this repo).

## Example
```
{
    "name": "tinynet",
    "version": "1.0.0",
    "homepage": "https://konradp.gitlab.io",
    "dependencies": {
       "gitlab.com/konradp/tinyhttpparser": "0.0.1",
       "gitlab.com/konradp/tinydate": "*"
    }
}
```

