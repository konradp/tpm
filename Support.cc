#include "Support.h"
#include <iostream>
#include <sys/stat.h>

namespace Support {

bool IsDir(std::string dir) {
    struct stat fileInfo;
    if(stat(dir.c_str(), &fileInfo) && S_ISDIR(fileInfo.st_mode))
        return true;
    else
        return false;
}

void MakeDir(std::string dir) {
    mkdir(
        dir.c_str(),
        S_IRWXU
        | S_IRGRP | S_IXGRP
        | S_IROTH | S_IXOTH
    );
    // TODO: Add error handling here
}

}; //namespace Support

