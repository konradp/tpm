#ifndef SUPPORT_H
#define SUPPORT_H

#include <string>

// A namespace for helper methods
namespace Support {

bool IsDir(std::string dir);
void MakeDir(std::string dir);

}; // namespace Support

#endif //SUPPORT_H
