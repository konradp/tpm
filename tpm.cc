#include <bits/stdc++.h> // for system()
#include <iostream>
#include "json/json.h"
#include "ConfigInterface.h"
#include "Support.h"

// Global vars
std::string kLibDir = "lib";

// Go to the lib dir and clone the repository
void
DownloadPkg(std::string name, std::string ver)
{
    // Prepare vars
    std::string url, cmd, opts;
    url = "http://" + name + ".git";

    opts = " --depth 1 --single-branch";
    if(ver != "*") {
        // A specific version (branch/tag) was specified
        opts += " --branch " + ver;
    }

    cmd = "cd " + kLibDir + "; git clone " + url + opts;

    // Clone
    system(cmd.c_str());

    // TODO: Error checking for e.g.:
    // - wrong version
}


int main()
{
    // Get list of packages
    ConfigInterface cfg = ConfigInterface("packages.json");
    Json::Value pkgs = cfg.GetPackages();

    // If kLibDir (./lib) is not a dir, create it
    if(!Support::IsDir(kLibDir)) {
        std::cout << "The " << kLibDir << " does not exist. "
                  << "Creating it." << std::endl;
        Support::MakeDir(kLibDir);
    }

    // Download each package dependency (git clone)
    if(pkgs.size() == 0) {
        std::cout << "No packages found. There is nothing to do." << std::endl;
    } else {
        for(auto name : pkgs.getMemberNames()) {
            DownloadPkg(name, pkgs[name].asString());
        }
    }
    return 0;
}

